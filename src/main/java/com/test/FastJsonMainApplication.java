package com.test;


import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;

@SpringBootApplication
public class FastJsonMainApplication {

    public static void main(String[] args) {

        SpringApplication.run(FastJsonMainApplication.class,args);


    }

    @Bean
    public HttpMessageConverters fastjsonHttpMessageCoverters() {
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(new SerializerFeature[]{ SerializerFeature.PrettyFormat });
        fastConverter.setFastJsonConfig(fastJsonConfig);
        FastJsonHttpMessageConverter fastJsonHttpMessageConverter1 = fastConverter;
        return new HttpMessageConverters(new HttpMessageConverter[]{(HttpMessageConverter) fastJsonHttpMessageConverter1});
    }
}
