package com.test.user;

import com.alibaba.fastjson.annotation.JSONField;

public class User {

    @JSONField
    private String name;
    @JSONField
    private int age;


    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setAge(int age){
        this.age=age;
    }

    public int getAge(){
        return this.age;
    }
}

