package com.test.controller;

import com.test.user.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FastJsonController {

    @ResponseBody
    @RequestMapping(value = "/hello")
    public String hello(){
        return "Hello World!";
    }

    @ResponseBody
    @RequestMapping(value = "/user", method = {RequestMethod.GET}, produces = {"application/json;charset=UTF-8"})
    public Object getUser(){
        User usr = new User();
        usr.setAge(20);
        usr.setName("Bob");
        return usr;
    }

    @RequestMapping(value = "/user", method = {RequestMethod.POST}, produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public Object setUser(@RequestBody User user){
        user.setAge(20);
        return user;
    }
}

